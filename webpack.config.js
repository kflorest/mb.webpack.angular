'use strict';
var webpack = require('webpack'),
path = require('path'),
WebPackAngularTranslate = require("webpack-angular-translate");

var PATHS = {
  APP: path.resolve(__dirname + '/app'),
  BOWER: path.resolve(__dirname + '/app/bower_components')
}

module.exports = {
  context: PATHS.APP,
  entry: {
    app: ['webpack/hot/dev-server', './core/bootstrap.js']
  },
  output: {
    path: PATHS.APP,
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    preLoaders: [
      {
        test: /\.html$/,
        loader: WebPackAngularTranslate.htmlLoader()
      },
      {
        test: /\/core\/directives\/.+script\.js$/,
        loader: 'baggage?[file].html&[file].css'
      }
    ],
    loaders: [
      {test: /\.scss$/,loader: 'style!css!sass', exclude: [/node_modules/]},
      {test: /\.css$/,loader: "style!css", exclude: [/node_modules/]},
      {test: /\.js$/,loader: 'ng-annotate!babel!jshint',exclude: /node_modules|bower_components/, exclude: [/node_modules/]},
      {test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,loader: 'file-loader?name=res/[name].[ext]?[hash]', exclude: [/node_modules/]},
      {test: /\.html$/, loader: 'html-loader', exclude: [/node_modules/] },
      //{test: /\.html$/,loader: 'ngtemplate?relativeTo=' + __dirname   + '/!html', exclude: [/node_modules/]},
      {test: /\.json$/,loader: 'json-loader', exclude: [/node_modules/]},
      {test: /\.ts$/,loader: WebPackAngularTranslate.jsLoader('typescript-simple-loader'), exclude: [/node_modules/]}
    ]
  },
  resolve: {
    root: __dirname,
    extensions: ['','.js','json']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new WebPackAngularTranslate.Plugin()
  ]
};
