var DATA = require('./data/config'); //Arrumara para que seja global
console.log(DATA.URL);
define(['./route.resolver'], function() {
  var app = angular.module('app', ['pascalprecht.translate','ngMaterial','ngRoute','ngResource','routeResolverServices']);
  app.config(['$translateProvider','$routeProvider','routeResolverProvider','$controllerProvider','$compileProvider','$filterProvider','$httpProvider','$provide',
	function ($translateProvider,$routeProvider,routeResolverProvider,$controllerProvider,$compileProvider,$filterProvider,$provide) {
      var tp = $translateProvider;
  		app.register =
      {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service
      };

  		var route = routeResolverProvider.route;
  		// angular.forEach(APP.DATA.CONFIG.URLS, function(obj){
  		// 	$routeProvider.when('/'+obj.temp, route.resolve(obj.temp));
  		// });
  		/*RUTA FIJAS*/
  		$routeProvider.when('/demo', route.resolve('demo'));
      $routeProvider.when('/home', route.resolve('home'));
  		$routeProvider.when('/404', route.resolve('404'));
  		$routeProvider.when('/', route.resolve('home'));

  		$routeProvider.otherwise({ redirectTo: '/home' });

      app.components = {
        controller: $controllerProvider.register,
        service: $provide.service
      };
      $translateProvider.useStaticFilesLoader({
        prefix: './core/language/lang-',
        suffix: '.json'
      });
      $translateProvider.preferredLanguage('es_PE');
	}]);
  app.directive('myDirective3', function() {
    return{
      restrict: 'E',
      scope: {},
      templateUrl: '<h1>HOLA QUE TAL</h1>'
    };
  });
  app.controller('appController', ['$scope','$rootScope','$location','$http','$translate', function($scope,$rootScope,$location,$http,$translate) {
	  var ng = $scope;
    ng.demo = 'HOLA SOY APP CONTROLLER';
    var vm = this;
    ng.changeLanguage = function(langkey) {
      $translate.use(langkey);
    };
	}]);
	return app;
});
