console.log('directive.js');
module.exports = function (app) {
  angular.module('app').directive('myDirective', function() {
    return{
      restrict: 'E',
      scope: {},
      templateUrl: './template.html',
      controller: function() {
        var vm = this;
        vm.greeting = "Hello Webpack!";
      },
      controllerAs: 'vm'
    };
  });
};
