console.log('indexDirectives.js');
define(['./modules/directives'],function (module) {
  module.directive('myDirective', function() {
    return{
      restrict: 'E',
      scope: {},
      templateUrl: '<h1>HOLA QUE TAL</h1>'
    };
  });
});
